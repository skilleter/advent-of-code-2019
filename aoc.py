################################################################################
''' Utility functions for AOC 2019
'''
################################################################################

import sys
import argparse
import logging

################################################################################

def readnumberfile(filename):
    """ Read a list of numbers from a file """

    with open(filename) as data:
        return [int(d) for d in data.readlines()]

################################################################################

def readfile(filename):
    """ Read a list of strings from a file """

    with open(filename) as data:
        return [d.strip() for d in data.readlines()]

################################################################################

def read_csv(filename):
    """ Read a list of CSV values from a file, return an array of
        an array of values on each line """

    values = []
    with open(filename) as data:
        for line in data:
            csv = line.strip()
            if csv:
                values.append([int(value) for value in csv.split(',')])

    return values

################################################################################

def error(msg):
    """ Report an error and exit """

    print(msg)
    sys.exit(1)

################################################################################

def init(filename=True, test=False):
    """ Generic initialisation function - takes commandline options,
        sets up logging and debug, reads a file of code and returns it """

    # Command lion arglemonsters

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Run with pudb')
    parser.add_argument('--verbose', action='store_true', help='Enable debug logging')

    if filename:
        parser.add_argument('filename', nargs=1, default='input.txt')

    if test:
        parser.add_argument('--test', action='store_true', help='Enable test code')

    args = parser.parse_args()

    # Optional logging and debuggery

    logging.basicConfig()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    if filename:
        args.filename = args.filename[0]

    if args.debug:
        import pudb
        pu.db

    return args
