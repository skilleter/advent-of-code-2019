#! /usr/bin/env python3

################################################################################
""" AoC Day 10 gold
"""
################################################################################

import sys
import pickle
import math
import copy

sys.path.append('..')

import aoc

################################################################################
# Initial moon states in x,y,z dx, dy, dz format

MOONS = \
[
    [  4,   1,  1, 0, 0, 0],
    [ 11, -18, -1, 0, 0, 0],
    [ -2, -10, -4, 0, 0, 0],
    [ -7, -2,  14, 0, 0, 0]
]

EXAMPLE_MOONS_1 = \
[
    [ -1,   0,  2, 0, 0, 0],
    [  2, -10, -7, 0, 0, 0],
    [  4,  -8,  8, 0, 0, 0],
    [  3,   5, -1, 0, 0, 0]
]

EXAMPLE_MOONS_2 = \
[
    [ -8, -10,  0, 0, 0, 0],
    [  5,   5, 10, 0, 0, 0],
    [  2,  -7,  3, 0, 0, 0],
    [  9,  -8, -3, 0, 0, 0]
]

################################################################################

def lcm(a, b):
    return (a * b) // int(math.gcd(a, b))

################################################################################

def main():
    """ Main function """

    global MOONS

    args = aoc.init(filename=False, test=True)

    if args.test:
        MOONS = EXAMPLE_MOONS_1

    start = copy.deepcopy(MOONS)

    print('INITIAL STATE')
    for moon in MOONS:
        print('    pos=<x=%3d, y=%3d, z=%3d> vel=<x=%3d, y=%3d, z=%3d>' % (moon[0], moon[1], moon[2], moon[3], moon[4], moon[5]))

    print()

    store_state = [[], [], []]
    completed = [0, 0, 0]

    step = 0
    done = 0

    while done < 3:
        for a in range(len(MOONS)):
            for b in range(a+1, len(MOONS)):
                for axis in range(3):

                    if MOONS[a][axis] < MOONS[b][axis]:
                        MOONS[a][axis + 3] += 1
                        MOONS[b][axis + 3] -= 1

                    elif MOONS[a][axis] > MOONS[b][axis]:
                        MOONS[a][axis + 3] -= 1
                        MOONS[b][axis + 3] += 1

        for moon in MOONS:
            for axis in range(3):
                moon[axis] += moon[axis + 3]

        step += 1

        print()
        print('Step #%d' % step)

        for moon in MOONS:
            print('    pos=<x=%3d, y=%3d, z=%3d> vel=<x=%3d, y=%3d, z=%3d>' % (moon[0], moon[1], moon[2], moon[3], moon[4], moon[5]))

        for axis in range(3):
            if not completed[axis]:
                if MOONS[0][axis] == start[0][axis] \
                and MOONS[1][axis] == start[1][axis] \
                and MOONS[2][axis] == start[2][axis] \
                and MOONS[0][axis+3] == start[0][axis+3] \
                and MOONS[1][axis+3] == start[0][axis+3] \
                and MOONS[2][axis+3] == start[0][axis+3]:
                    print('Axis %d repeats after %d steps' % (axis, step))
                    done += 1
                    completed[axis] = step

    for axis in range(3):
        print('Axis %d has a cycle of %d' % (axis, completed[axis]))

    result = lcm(lcm(completed[0], completed[1]), completed[2])

    print('Result = %d' % result)

################################################################################
# Entry point

main()
