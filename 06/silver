#! /usr/bin/env python3

################################################################################
""" AoC Day 6 silver & gold
"""
################################################################################

import sys
sys.path.append('..')

import aoc

################################################################################

class Orbit:
    """ Linked list node for each planet - contains the names of the planet,
        what it is orbitting and a link to the node it is orbitting """

    def __init__(self, name, orbit):
        self.name = name
        self.orbit = orbit
        self.link = None
        self.distance = 0

    def __repr__(self):
        return '%s orbits %s' % (self.name, self.orbit)

################################################################################

def initialise(filename):
    """ Read the input data, generate the tree structure and return the array
        of objects. """

    # Read the list of orbits, splitting into orbiter and orbitee

    orbits = [o.split(')') for o in aoc.readfile(filename)]

    # Build a list of orbit nodes

    orbitdata = []
    objects = []

    for o in orbits:
        print('Adding %s, orbitting %s' % (o[1], o[0]))
        objects.append(o[1])
        orbitdata.append(Orbit(o[1], o[0]))

    # Now add any objects that don't orbit anything

    for o in orbits:
        if o[0] not in objects:
            print('Adding %s which does not orbit anything' % o[0])
            orbitdata.append(Orbit(o[0], None))

    # For each object, link its node to the object node it is orbitting

    for o in orbitdata:
        for p in orbitdata:
            if p.name == o.orbit:
                if o.link:
                    aoc.error('%s is already orbitting %s - cannot make it orbit %s' % (o.name, o.link.name, p.name))

                o.link = p
                break

    for o in orbitdata:
        if not o.link:
            print('%s is not orbitting anything' % o.name)

    return orbitdata

################################################################################

def silver(orbitdata):
    """ Silver: for each object, count the number of things it is orbitting
        and total them all """

    count = 0
    for o in orbitdata:
        p = o
        c = 0

        while p.link:
            c += 1
            p = p.link

        print('%s is orbitting %d objects in total' % (o.name, c))
        count += c

    print('Total orbit count = %d' % count)

################################################################################

def gold(orbitdata):
    """ Now the gold part - label everything down the path from Santa
        with a Santa distance # then count down from our location
        until we find one with a Santa distance and add the two """

    you = None
    santa = None

    for o in orbitdata:
        if o.name == 'YOU':
            you = o
        elif o.name == 'SAN':
            santa = o

        if santa and you:
            break

    if not santa:
        aoc.error('Where is Santa?')

    if not you:
        aoc.error('Where are you?')

    print(santa)
    print(you)

    distance = 0
    while santa:
        santa.distance = distance
        distance += 1
        santa = santa.link

    distance = 0
    while not you.distance:
        you = you.link
        distance += 1

    # Total distance ignores our orbit and santa since we need to know
    # the number of transfers

    total_distance = distance + you.distance - 2

    print('Distance to santa = %d' % total_distance)

################################################################################
# Entry

if len(sys.argv) > 1:
    datafilename = sys.argv[1]
else:
    datafilename = 'input.txt'

orbitdata = initialise(datafilename)

silver(orbitdata)
gold(orbitdata)
