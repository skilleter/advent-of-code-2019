#! /usr/bin/env python3

################################################################################
''' AOC Day 3 - Gold
'''
################################################################################

import sys
sys.path.append('..')

import aoc

################################################################################

# Lookup table to translate directions to X/Y delta values

DELTA = {'U': [0, 1], 'D': [0, -1], 'L': [-1, 0], 'R': [1, 0]}

################################################################################

def walk(route, other_distances):
    """ Similar to silver (cf), but now returns the shortest total distance to an
        intersection, rather than the nearest intersection to the origin """

    x = y = steps = 0

    instructions = route.strip().split(',')
    shortest = 0
    distances = {}

    for move in instructions:
        direction = move[0]
        distance = int(move[1:])

        dx = DELTA[direction][0]
        dy = DELTA[direction][1]

        while distance:
            x += dx
            y += dy

            distance -= 1
            steps += 1

            if not other_distances:
                if x in distances:
                    if y not in distances[x]:
                        distances[x][y] = steps
                else:
                    distances[x] = {y:steps}

            else:
                if x in other_distances and y in other_distances[x]:
                    total_distance = steps + other_distances[x][y]

                    if (not shortest or total_distance < shortest):
                        print('New shortest distance at (%d, %d) total distance = %d' % (x, y, total_distance))

                        shortest = total_distance

    return shortest, distances

################################################################################
# Entry point

if len(sys.argv) > 1:
    datafilename = sys.argv[1]
else:
    datafilename = 'input.txt'

print('Reading %s' % datafilename)

with open(datafilename, 'r') as datafile:
    data = datafile.readlines()

print('Calculating first route')

_, distances = walk(data[0], None)

print('Route = %d steps' % len(distances))

print('Calculating second route and looking for intersections')

shortest, _ = walk(data[1], distances)

print('Shortest = %d' % shortest)
