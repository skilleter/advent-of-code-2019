#! /usr/bin/env python3

import threading
import queue

################################################################################
""" Intcode interpreter for AoC 2019

An Intcode program is a list of integers separated by commas (like
1,0,0,3,99). To run one, start by looking at the first integer (called
position 0). Here, you will find an opcode - either 1, 2, or 99. The
opcode indicates what to do; for example, 99 means that the program is
finished and should immediately halt. Encountering an unknown opcode
means something went wrong.

    Opcode 1 ADD - adds together numbers read from two positions and stores the result in a third position.
             The three integers immediately after the opcode tell you these three positions -
             the first two indicate the positions from which you should read the input values,
             and the third indicates the position at which the output should be stored.

    Opcode 2 MULT - works exactly like opcode 1, except it multiplies the two inputs instead of adding them.
             Again, the three integers after the opcode indicate where the inputs and outputs are, not their values.

    Opcode 3 STORE - takes a single integer as input and saves it to the position given by its only parameter.
             For example, the instruction 3,50 would take an input value and store it at address 50.

    Opcode 4 OUTPUT - outputs the value of its only parameter.
             For example, the instruction 4,50 would output the value at address 50.

    Opcode 5 JUMPTRUE - is jump-if-true:
             If the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter.
             Otherwise, it does nothing.

    Opcode 6 JUMPFALSE - is jump-if-false:
             if the first parameter is zero, it sets the instruction pointer to the value from the second parameter.
             Otherwise, it does nothing.

    Opcode 7 LESSTHAN - is less than:
             if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter.
             Otherwise, it stores 0.

    Opcode 8 EQUALS - is equals:
             if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter.
             Otherwise, it stores 0.

    Opcode 9 ADJRELATIVE - Adjust the current relative base
             adds the parameter (which may be positive or negative) the current relative base address register

    Open 99 END - Ends execution

    Parameter modes are stored in the same value as the instruction's
    opcode.

    The opcode is a two-digit number based only on the ones and tens
    digit of the value, that is, the opcode is the rightmost two
    digits of the first value in an instruction.

    Parameter modes are single digits, one per parameter, read
    right-to-left from the opcode: the first parameter's mode is in
    the hundreds digit, the second parameter's mode is in the
    thousands digit, the third parameter's mode is in the
    ten-thousands digit, and so on. Any missing modes are 0.

    Each parameter of an instruction is handled based on its parameter
    mode.

    Parameter mode 0, POSITION MODE:

        Causes the parameter to be interpreted as a position - if the
        parameter is 50, its value is the value stored at address 50
        in memory.

    Parameter mode 1, IMMEDIATE MODE.

        In immediate mode, a parameter is interpreted as a value - if
        the parameter is 50, its value is simply 50.

    Parameters that an instruction writes to will never be in
    immediate mode.
"""
################################################################################

import sys
import logging
import copy

################################################################################

# Opcodes of instructions that write to memory

WRITE_OPCODES = (1, 2, 3, 7, 8)

################################################################################

class IntCode:
    """ IntCode interpreter """

    def __init__(self, code, input_fn, output_fn):
        self.code = copy.copy(code)
        self.relative = 0
        self.input_fn = input_fn
        self.output_fn = output_fn
        self.log = logging.getLogger('CPU')
        self.log.setLevel(logging.INFO)

    @staticmethod
    def __error(msg):
        """ Output a fatal error then quit """

        print(msg)
        sys.exit(1)

    def setlogging(self, level):
        """ Set CPU logging level """

        self.log.setLevel(level)

    def __extendmem(self, addr):
        """ Extend memory to include the specified address, reporting
            a fatal error if the address is negative """

        if addr < 0:
            self.__error('Fatal error - invalid address: %d' % addr)

        if addr >= len(self.code):
            self.code += [0] * (addr - len(self.code) + 1)

    def __read(self, addr):
        """ Read a value from memory, extending memory if the address is
            positive and out of bounds """

        self.__extendmem(addr)

        return self.code[addr]

    def __write(self, addr, value):
        """ Write a value to  memory, extending memory if the address is
            positive and out of bounds """

        self.__extendmem(addr)

        self.code[addr] = value

    def __get_param(self, addr, mode):
        """ Read a parameter from memory """

        if mode == 0:
            # Position mode - indirect read

            return self.__read(self.__read(addr))
        elif mode == 1:
            # Parameter mode - direct read

            return self.__read(addr)
        elif mode == 2:
            # Relative mode - position mode with offset

            return self.__read(self.__read(addr) + self.relative)
        else:
            self.__error('Invalid mode at %d: %d' % (addr, mode))

    def __set_value(self, addr, mode, value):
        """ Write a value to memory """

        if mode == 0:
            self.__write(self.__read(addr), value)

        elif mode == 2:
            self.__write(self.__read(addr) + self.relative, value)

        else:
            self.__error('Invalid mode at %d: %d' % (addr, mode))

    def set(self, addr, value):
        """ Public function to write to memory """

        self.__write(addr, value)

    def get(self, addr):
        """ Public function to read from memory """

        return self.__read(addr)

    def run(self):
        """ Run code, given the code and I/O functions """

        self.log.info('Executing code of length %d', len(self.code))

        # Initialise the program counter

        pc = 0

        # Execute forever (until we hit a stop instruction)

        while True:
            opcode = self.code[pc] % 100
            mode = self.code[pc] // 100

            mode1 = mode % 10
            mode2 = (mode // 10) % 10
            mode3 = (mode // 100) % 10

            self.log.debug('@%d : O:%2d m1: %2d m2: %2d m3: %2d', pc, opcode, mode1, mode2, mode3)

            if opcode == 99:
                # End execution

                self.log.debug('    EXIT')
                break

            if opcode in WRITE_OPCODES and mode3 == 1:
                self.__error('Invalid mode (%d) for write instruction at %d' % (mode3, pc))

            if opcode == 1:
                # C = A + B

                x = self.__get_param(pc+1, mode1)
                y = self.__get_param(pc+2, mode2)

                self.log.debug('    ADD %d %d', x, y)

                self.__set_value(pc+3, mode3, x+y)
                pc += 4

            elif opcode == 2:
                # C = A * B

                x = self.__get_param(pc+1, mode1)
                y = self.__get_param(pc+2, mode2)

                self.log.debug('    MULT %d %d', x, y)

                self.__set_value(pc+3, mode3, x*y)
                pc += 4

            elif opcode == 3:
                # A = INPUT

                self.log.debug('    INPUT')

                if not self.input_fn:
                    self.__error('    INPUT opcode and no output function defined - terminated execution')

                input_value = self.input_fn()
                if input_value is None:
                    print('ERROR: No input')
                    break

                self.__set_value(pc+1, mode1, input_value)
                pc += 2

            elif opcode == 4:
                # OUTPUT = B

                self.log.debug('    OUTPUT')

                if not self.output_fn:
                    self.__error('    OUTPUT opcode and no output function defined - terminated execution')

                if not self.output_fn(self.__get_param(pc+1, mode1)):
                    self.log.debug('    Terminated by output function')
                    break

                pc += 2

            elif opcode == 5:
                # Jump if TRUE

                cond = self.__get_param(pc+1, mode1)

                self.log.debug('    JMPT %d', cond)

                if cond:
                    pc = self.__get_param(pc+2, mode2)
                else:
                    pc += 3

            elif opcode == 6:
                # Jump if FALSE

                cond = self.__get_param(pc+1, mode1)

                self.log.debug('    JMPF %d', cond)

                if not cond:
                    pc = self.__get_param(pc+2, mode2)
                else:
                    pc += 3

            elif opcode == 7:
                # Less than

                x = self.__get_param(pc+1, mode1)
                y = self.__get_param(pc+2, mode2)

                self.log.debug('    LT %d %d', x, y)

                self.__set_value(pc+3, mode3, 1 if x < y else 0)

                pc += 4

            elif opcode == 8:
                # Equals

                x = self.__get_param(pc+1, mode1)
                y = self.__get_param(pc+2, mode2)

                self.log.debug('    EQ %d %d', x, y)

                self.__set_value(pc+3, mode3, 1 if x == y else 0)

                pc += 4

            elif opcode == 9:
                # Set relative base

                x = self.__get_param(pc+1, mode1)

                self.log.debug('    SETREL %d' % x)

                self.relative += x

                pc += 2

            else:
                self.__error('Invalid opcode: %d' % opcode)

################################################################################

#class ThreadedIntCode(IntCode):
    
#    def __init_(self, code, input_queue, output_queue):
        
